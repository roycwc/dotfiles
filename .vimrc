set nocompatible
set number relativenumber
set hls incsearch
syntax on
set ts=2
set sw=2
set et 
set ai

filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'terryma/vim-smooth-scroll'
Plugin 'AutoComplPop'

call vundle#end()
filetype plugin indent on

inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {}     {}


inoremap (      ()<Left>
inoremap (<CR>  (<CR>)<Esc>O
inoremap ()     ()


noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 2, 1)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 2, 1)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 2, 1)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 2, 1)<CR>
inoremap <expr> <Tab> pumvisible() ? "\<CR>" : "\<Tab>"
